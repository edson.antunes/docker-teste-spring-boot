package dockertestespringboot.dockertestespringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerTesteSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerTesteSpringBootApplication.class, args);
	}

}
